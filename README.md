# Physics Library

This is a KiCAD library I have created for my own use.

It contains components used in physics applications.

Currently it contains:
* symbol and footprint for connecting wires for a Delay Wire Chamber:
  * 5cm chamber with 2mm wire spacing
* operational amplifier symbol copy for OPA694xDBV
* TSFP-4-1 footprint with pads 2 and 4 common:
  * For BFP840FESD device

